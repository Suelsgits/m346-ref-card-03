# Use an official Maven image as a parent image
FROM maven:3.8.6-openjdk-11 AS build

# Set the working directory in the container
WORKDIR /app

# Copy the pom.xml file to the container
COPY pom.xml ./

# Install dependencies
RUN mvn dependency:go-offline

# Copy the rest of the application code to the container
COPY src ./src

# Build the project
RUN mvn package

# Use a minimal Java runtime for the final image
FROM openjdk:11-jre-slim

# Set the working directory in the container
WORKDIR /app

# Copy the packaged jar from the build stage
COPY --from=build /app/target/architecture-refcard-03-0.0.1-SNAPSHOT.jar ./architecture-refcard-03-0.0.1-SNAPSHOT.jar

# Set environment variables for the database
ENV DB_USERNAME=jokedbuser
ENV DB_PASSWORD=123456
ENV DB_URL=jdbc:postgresql://localhost:5432/mydatabase

# Run the application
CMD ["java", "-jar", "architecture-refcard-03-0.0.1-SNAPSHOT.jar"]
